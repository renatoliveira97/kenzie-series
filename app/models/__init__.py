from app.exceptions.series_exceptions import (
    ObjectVariableIsNotInstanceOfRightClassException, 
    UserNotFoundException
)
import psycopg2
from psycopg2 import sql
from environs import Env

env = Env()
env.read_env()

db_host = env('DB_HOST')
db_name = env('DB_NAME')
db_user = env('DB_USER')
db_password = env('DB_PASSWORD')
 
class Serie():

    FIELDNAMES = ["id", "serie", "seasons", "released_date", "genre", "imdb_rating"]

    def __init__(self, serie, seasons, released_date, genre, imdb_rating):
        self.serie = serie.title()
        self.seasons = seasons
        self.released_date = released_date
        self.genre = genre.title()
        self.imdb_rating = imdb_rating

    
    @staticmethod
    def save(serie):
        
        conn, cur = Serie.database_connection()

        Serie.create_table_if_not_exists(cur)

        new_serie = list(serie.__dict__.values())

        query = sql.SQL("""
            INSERT INTO
                ka_series
            (serie, seasons, released_date, genre, imdb_rating)
            VALUES
                ({serie_values});
        """).format(serie_values = sql.SQL(',').join(sql.Literal(value) for value in new_serie))

        cur.execute(query)

        Serie.close_database_connection(conn, cur)


    @staticmethod
    def get_all():

        conn, cur = Serie.database_connection()

        Serie.create_table_if_not_exists(cur)

        cur.execute("""
            SELECT * FROM ka_series;
        """)

        data = cur.fetchall()

        processed_data = [dict(zip(Serie.FIELDNAMES, row)) for row in data]

        Serie.close_database_connection(conn, cur)

        return processed_data


    @staticmethod
    def get_serie_by_id(id):

        conn, cur = Serie.database_connection()

        Serie.create_table_if_not_exists(cur)

        query = sql.SQL("""
            SELECT * FROM ka_series WHERE id = {serie_id};
        """).format(serie_id = sql.Literal(id))

        cur.execute(query)

        data = cur.fetchall()

        processed_data = [dict(zip(Serie.FIELDNAMES, row)) for row in data]

        if processed_data == []:

            raise UserNotFoundException()

        Serie.close_database_connection(conn, cur)

        return processed_data


    @staticmethod
    def database_connection():

        conn = psycopg2.connect(host=db_host, database=db_name,
                                user=db_user, password=db_password)

        cur = conn.cursor()

        return conn, cur


    @staticmethod
    def create_table_if_not_exists(cur):

        query = sql.SQL("""
            CREATE TABLE IF NOT EXISTS ka_series(
                "id" BIGSERIAL PRIMARY KEY,
                "serie" VARCHAR(100) NOT NULL UNIQUE,
                "seasons" INTEGER NOT NULL,
                "released_date" DATE NOT NULL,
                "genre" VARCHAR(50) NOT NULL,
                "imdb_rating" FLOAT NOT NULL
            );
        """)

        cur.execute(query)


    @staticmethod
    def close_database_connection(conn, cur):

        conn.commit()

        cur.close()

        conn.close()


    @staticmethod
    def check_if_the_variables_is_instance_of_right_classes(
        serie, seasons, released_date, genre, imdb_rating
        ):

        print(type(released_date))

        if (not isinstance(serie, str) or not isinstance(seasons, int) 
            or not isinstance(released_date, str) or not isinstance(genre, str) 
            or not isinstance(imdb_rating, float)):

            raise ObjectVariableIsNotInstanceOfRightClassException()             
