from flask import Flask
from app.routes import series_route


def create_app():

    app = Flask(__name__)

    series_route(app)

    return app
