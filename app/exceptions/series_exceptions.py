class ObjectVariableIsNotInstanceOfRightClassException(Exception):
    pass


class UserNotFoundException(Exception):
    pass
