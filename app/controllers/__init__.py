from app.exceptions.series_exceptions import (
    ObjectVariableIsNotInstanceOfRightClassException,
    UserNotFoundException
)
from psycopg2.errors import UniqueViolation
from app.models import Serie
from flask import jsonify, request


def create():

    try:

        data = request.get_json()

        Serie.check_if_the_variables_is_instance_of_right_classes(**data)

        serie = Serie(**data)

        Serie.save(serie)

        return jsonify(serie.__dict__), 201

    except ObjectVariableIsNotInstanceOfRightClassException:
        return jsonify({"error": "An object variable is not an instance of right class."}), 400

    except TypeError as e:
        return jsonify({"error": f"{e.args}"}), 400


def series():

    data = Serie.get_all()

    return jsonify(data), 200


def select_by_id(id):

    try:

        data = Serie.get_serie_by_id(id)

        data_to_dict = {"serie": dict(zip(data[0].keys(), data[0].values()))}

        return data_to_dict, 200

    except UserNotFoundException:

        return {"error": "Not Found"}, 404
