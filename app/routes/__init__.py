from flask import Flask
from app.controllers import create, series, select_by_id


def series_route(app: Flask):
    
    app.post('/series')(create)
    app.get('/series')(series)
    app.get('/series/<int:id>')(select_by_id)
